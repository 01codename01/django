--
-- ���� ������������ � ������� SQLiteStudio v3.2.1 � �� ��� 6 16:08:17 2021
--
-- �������������� ��������� ������: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- �������: product
CREATE TABLE product (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR (150));
INSERT INTO product (id, name) VALUES (1, 'product1');
INSERT INTO product (id, name) VALUES (2, 'product2');
INSERT INTO product (id, name) VALUES (3, 'product3');
INSERT INTO product (id, name) VALUES (4, 'product4');
INSERT INTO product (id, name) VALUES (5, 'product5');
INSERT INTO product (id, name) VALUES (6, 'product6');

-- �������: product_on_sklady
CREATE TABLE product_on_sklady (id INTEGER PRIMARY KEY NOT NULL, skladyId INTEGER REFERENCES Sklady (id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, productId INTEGER REFERENCES product (id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, quantity INTEGER (1000000));
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (1, 1, 1, 10);
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (2, 2, 1, 5);
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (3, 3, 1, 7);
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (4, 1, 2, 9);
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (5, 2, 2, 3);
INSERT INTO product_on_sklady (id, skladyId, productId, quantity) VALUES (6, 3, 3, 4);

-- �������: Sklady
CREATE TABLE Sklady (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR (150));
INSERT INTO Sklady (id, name) VALUES (1, 'sklad1');
INSERT INTO Sklady (id, name) VALUES (2, 'sklad2');
INSERT INTO Sklady (id, name) VALUES (3, 'sklad3');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
