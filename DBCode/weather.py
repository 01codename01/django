import requests
morn=[] #список с прогнозируемыми утренними значениями
try:
    res = requests.get("http://api.openweathermap.org/data/2.5/onecall",
                        params={'lat': '59.8944', 'lon': '30.2642', 'exclude': 'current,minutely,hourly,alerts',
                        'units': 'metric', 'APPID': '5c3833cc4340d7639079530906b51c85'})
    data = res.json()
    for i in data['daily'][0:5]:
        morn.append(i['temp']['morn'])
    print('Прогнозная утренняя температура на пять предстоящих дней:')
    print('Cредняя:', round((sum(morn)/5), 3), u'\N{DEGREE SIGN}C')
    print('Максимальная:', max(morn), u'\N{DEGREE SIGN}C')
except Exception as e:
    print("Что-то пошло не так, проверьте данные!")