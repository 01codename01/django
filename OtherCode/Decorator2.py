def first_decorator(func):
    def wrapped():
        print('Inside first decorator product')
        return func()
    return wrapped

def second_decorator(func):
    def wrapped():
        print('Inside second decoraotr product')
        return func()
    return wrapped

@first_decorator
@second_decorator
def decorated():
    print('Finally called')

decorated()

def accumulator():
    total=0
    while True:
        value = yield total
        print('Got = {} Total = {}'.format(value, total))
        if not value:
            break
        total+=value

generator=accumulator()
print(next(generator))

print('Acc = {}'.format(generator.send(1)))
print('Acc = {}'.format(generator.send(4)))
print('Acc = {}'.format(generator.send(4)))