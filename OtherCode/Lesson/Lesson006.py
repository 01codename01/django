# from re import *
# from math import sqrt

# result = findall(r'[0-9]+', input())
# result.reverse()
#
# for i in result:
#     print('{:.4f}'.format(sqrt(int(i))))
#

import re
#
# a = ''' 1427  0\n\n    876652098643267843\n 5276538'''
#
# print(*(['{0:.4f}'.format(int(x)**0.5) for x in a.split()[::-1]]), sep='\n')
#

# lst = []
#
# import sys
# for line in sys.stdin:
#     print(*(['{0:.4f}'.format(int(x) ** 0.5) for x in line.split()[::-1]]), sep='\n')

import sys

arr = []
for line in sys.stdin:
    arr.extend(line.split())

for x in arr[::-1]:
    print('{0:.4f}'.format(int(x) ** 0.5))


# import sys, math
#
# a = []
# [a.extend(i.split()) for i in
#  sys.stdin]  # Считывает данные и записывает в массив, до перевода строки. Далее заполняется другой массив, пока не встретится еще один перевод строки, или пока не закончатся данные, и все полученные массивы объединяются в 1 массив
# print(*(str(int(x) ** 0.5) for x in a[::-1]), sep='\n')

# import re
# pattern = r"[0-9]+"
# number_re = re.compile(pattern)
# num = number_re.findall(input())
# print(num)