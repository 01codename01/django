
# for n in range(0,1002):
# print('n=', n, end=' ')
# if (n in range(1,10)):
#     print('few')
#
# elif (5 <= n <= 9):
#     print('several')
#
# elif (10 <= n <= 19):
#     print('pack')
#
# elif (20 <= n <= 49):
#     print('lots')
#
# elif (50 <= n <= 99):
#     print('horde')
#
# elif (100 <= n <= 249):
#     print('throng')
#
# elif (250 <= n <= 499):
#     print('swarm')
#
# elif (500 <= n <= 999):
#     print('zounds')
#
# elif (n >= 1000):
#     print('legion')

n = int(input())
truth_table = {
    'few': lambda x: x > 0 and x < 5,
    'several': lambda x: x > 4 and x < 10,
    'pack': lambda x: x > 9 and x < 20,
    'lots': lambda x: x > 19 and x < 50,
    'horde': lambda x: x > 49 and x < 100,
    'throng': lambda x: x > 99 and x < 250,
    'swarm': lambda x: x > 249 and x < 500,
    'zounds': lambda x: x > 499 and x < 1000,
    'legion': lambda x: x > 999
}

for key, val in truth_table.items():
    if val(n):
        print(key)