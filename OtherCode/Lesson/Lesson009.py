class posIterator:
    def __iter__(self):
        return self
    def __init__(self, lst):
        self.lst = lst
        self.counter = 0
    def __next__(self):
        if self.counter < len(self.lst):
            self.counter += 1
            if self.lst[self.counter-1] > 0:
                return self.lst[self.counter-1]
            else:
                return 'Хрен там!'
        else:
            raise StopIteration


a = [1, -2, -3, 4, 5, -6, 7, 8, -9, 10]


i = posIterator(a)
for n in i:
    if n != None:
        print(n)
