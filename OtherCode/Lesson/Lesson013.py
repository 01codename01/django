import re, requests

# url1 = r'https://stepic.org/media/attachments/lesson/24472/sample0.html'
# url2 = r'https://stepic.org/media/attachments/lesson/24472/sample2.html'
url1 = input()
url2 = input()
result = False
# ur1 = [re.search(r'https://.*\.html',requests.get(url1).text).group()]
ur1 = requests.get(url1)
if ur1.status_code == 200:
    for link in re.findall(r'https://.*\.html', ur1.text):
        res = requests.get(link)
        if res.status_code == 200:
            for ur in re.findall(r'https://.*\.html', res.text):
                if url2 == ur:
                    result = True
                    break
            if result:
                break
else:
    result = False
if result:
    print('Yes')
else:
    print('No')


# import re
# import requests
#
# start_url = input()
# end_url = input()
#
# found = False
#
# link_pattern = re.compile(r'<a[^>]*?href="(.*?)"[^>]*?>')
#
# resp = requests.get(start_url).text
# for url in link_pattern.findall(resp):
#     cur_resp = requests.get(url).text
#     if end_url in link_pattern.findall(cur_resp):
#         found = True
#         break
#
# print("Yes" if found else "No")


# import requests, re
# A, B, T = input(), input(), re.compile(r'<a\s+href\s*=\s*"(.+)"')
# print("Yes" if B in (D for C in T.findall(requests.get(A).text) for D in T.findall(requests.get(C).text)) else "No")

# import requests
# import re
#
# Atext, urlB = requests.get(input()).text, input()
# for href in re.findall(r'<a .*href="(.*)">.*</a>', Atext):
#     response = requests.get(href)
#     if response.status_code == 200 and urlB in re.findall(urlB, response.text):
#         print('Yes')
#         exit()
# print('No')
