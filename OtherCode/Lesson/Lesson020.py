def SearchInSortedList (x, a):
    f, l = 0, len (a) - 1
    n = 0
    if x not in a:
        return -1
    else:
        while a[f] != x:
            if a[(f + l) // 2] < x:
                f = (f + l) // 2 + 1
            else:
                f = (f + l) // 2
            n+=1
    return f,n


print(SearchInSortedList(int(input()), list(map(int, input().split()))))