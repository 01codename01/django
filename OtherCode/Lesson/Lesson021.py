class DictAdder:
    def __init__(self, start={}):
        self.data=start
        for k in start.keys(): self.data[k] = start[k]
    def __add__(self, other):
        for k in other.keys(): self.data[k] = other[k]
        return self.data
    def __repr__(self):
        return repr(self.data)
ld = DictAdder()

print(ld+{2:2})