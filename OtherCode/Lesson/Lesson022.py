import math


def fib(n): return n >= 0 and (n == 0 or math.sqrt(5 * n * n - 4).is_integer() or math.sqrt(5 * n * n + 4).is_integer())


b = 10000
i = 1
while i <= 10000 and b != x:
    if fib(i):
        b = b - i
    else:
        b = b + 1
    i += 1
return i - 1
