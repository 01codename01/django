class Set:
    def __init__(self, value = []):
        self.data = []
        self.concat(value)
    def intersect(self, other):
        res = []
        for x in self.data:
            if x in other:
                res.append(x)
        return Set(res)
    def union(self, other):
        res = self.data[:]
        for x in other:
            if x in other:
                if not x in res:
                    res.append(x)
        return Set(res)
    def concat(self, value):
        for x in value:
            if not x in self.data:
                self.data.append(x)
    def __len__(self): return len(self.data)
    def __getitem__(self, key): return self.data[key]
    def __and__(self, other): return self.intersect(other)
    def __or__(self, other): return self.union(other)
    def __repr__(self): return 'Set: ' + repr(self.data)
x = Set([1,2,3,4])
y = Set([3,4,5])
print(x & y, x | y)
z = Set("Hello")
print(z[0], z[-1])
for c in z:
    print(c, end = ' ')
print(len(z), z)
print(z & "mello", z | "mello")