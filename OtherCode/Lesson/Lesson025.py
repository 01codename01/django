class Lunch:
    def __init__(self):
        self.cust = Customer()
        self.emp = Employee()
    def order(self, foodName):
        self.cust.placeOrder(foodName, self.emp)
    def result(self):
        self.cust.printFood()
class Customer:
    def __init__(self):
        self.food = None
    def placeOrder(self, foodName, employee):
        self.food = employee.takeOrder(foodName)
    def printFood(self):
        print(self.food.name)
class Employee:
    def takeOrder(self, foodName):
        return Food(foodName)
class Food:
    def __init__(self, name):
        self.name = name

x = Lunch()
x.order('Chicken')
x.result()