class Animal:
    def reply(self): self.speak()
    def speak(self): print('Spam')
class Mammal(Animal):
    def speak(self): print('Huh?')
class Cat(Mammal):
    def speak(self): print('Meow!')
class Dog(Mammal):
    def speak(self): print('Bark!')
class Primate(Mammal):
    def speak(self): print('Hello world!')
x = Cat()
x.reply()
