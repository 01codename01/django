def ways(number):
    if number < 2:
        return 0
    elif number == 2:
        return 1
    elif number % 2 != 0:
        return ways(number - 1)
    else:
        return ways(number - 1) + ways(number // 2)
print(ways(22))