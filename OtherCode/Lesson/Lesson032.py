def f(n):
   return n * n * n

for k in range(0,100):
    i = 1
    while f(i) < k:
        i = i + 1
    if f(i) - k <= k - f(i - 1):
        print(i)
    else:
        print(i - 1)