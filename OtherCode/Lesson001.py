for i in range((-20000), (2**31)):
    x = int(i)
    a = 0
    b = 0
    while x > 0:
        c = x % 2
        if c == 0:
            a = a + 1
        else:
            b = b + 1
        x = x // 10
    if (a==2 and b==3):
        print("i = ", i, "a = ", a, "b = ", b)
