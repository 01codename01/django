

x = float(input())
y = float(input())
oper = input()

if(oper == '/' and y==0):
    print('Деление на 0!')
elif(oper == '/' and y!=0):
    print(x/y)
elif(oper == '+'):
    print(x+y)
elif(oper == '-'):
    print(x-y)
elif(oper == '*'):
    print(x*y)
elif (oper == 'mod' and y == 0):
    print('Деление на 0!')
elif(oper == 'mod' and y!=0):
    print(x % y)
elif(oper == 'pow'):
    print(x ** y)
elif(oper == 'div' and y == 0):
    print('Деление на 0!')
elif(oper == 'div' and y != 0):
    print(x // y)