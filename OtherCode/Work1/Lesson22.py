num = int(input())
num1 = 1
if (num != 0):
    if (num == 1):
        print(1, end='')
    elif (num == 2):
        print(1, 2, end='')
    else:
        for i in range(num):
            for j in range(0, i):
                if (num1 <= num):
                    print(i, end='')
                    if (num1 != num):
                        print(" ", end='')
                    num1 += 1

print('')
string = ''
for i in range(num):
    string += str(i) * i

for i in range(num):
    print(int(string[i]), end='')
    if (i != num):
        print(" ", end='')

i = 0
while i < 5:
    print('*')
    if i % 2 == 0:
        print('**')
    if i > 2:
        print('***')
    i = i + 1