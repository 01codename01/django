lst = [int(i) for i in input().split()]
tmp = []
for i in lst:
    if lst.count(i) > 1:
        if (i not in tmp):
            print(i, end=' ')
            tmp.append(i)
        else:
            continue
