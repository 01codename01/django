a = []
s = []
while 'end' not in a:
    a = [i for i in input().split()]
    s.append(a)
s.remove(['end'])

for i in range(0, len(s)):
    for j in range(0, len(s[i])):
        if (i < (len(s) - 1) and j < len(s[i]) - 1):
            print(int(s[i - 1][j]) + int(s[i + 1][j]) + int(s[i][j - 1]) + int(s[i][j + 1]), end = ' ')
        elif (i < (len(s) - 1) and j == len(s[i]) - 1):
            print(int(s[i - 1][j]) + int(s[i + 1][j]) + int(s[i][j - 1]) + int(s[i][0]), end = ' ')
        elif (i == (len(s) - 1) and j < len(s[i]) - 1):
            print(int(s[i - 1][j]) + int(s[0][j]) + int(s[i][j - 1]) + int(s[i][j + 1]), end = ' ')
        elif (i == (len(s) - 1) and j == len(s[i]) - 1):
            print(int(s[i - 1][j]) + int(s[0][j]) + int(s[i][j - 1]) + int(s[i][0]), end = ' ')
    print('')
