str1 = input()
str2 = input()
str3 = input()
str4 = input()
# str1 = 'abcd'
# str2 = '*d%#'
dct_cr = {key: value for key, value in zip(str1, str2)}
dct_enc = {value: key for key, value in dct_cr.items()}

# str3 = 'abacabadaba'
# str4 = '#*%*d*%'
for i in str3:
    if i in dct_cr:
        print(dct_cr[i], end='')
print('\n',end='')
for i in str4:
    if i in dct_enc:
        print(dct_enc[i], end='')
