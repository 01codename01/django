dct = {}
lst = []
for key in range(1, 12):
    dct[key] = [0, 0]
with open('file1.txt', 'r') as f:
    for str in f.readlines():
        lst.append(str.strip().split('\t'))
with open('file2.txt', 'w') as f:
    for num in range(len(lst)):
        str = dct[int(lst[num][0])]
        str[0] = str[0] + int(lst[num][2])
        str[1] = str[1] + 1
    for key in range(1, 12):
        str1 = dct[key]
        if (str1[1] != 0):
            f.write('{} {}\n'.format(key, (float(str1[0] / str1[1]))))
        else:
            f.write('{} -\n'.format(key))
