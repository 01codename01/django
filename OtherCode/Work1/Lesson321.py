def modify_list(l):
    i = 0
    while i < len(l):
        print(i, l[i], l[i] % 2)
        if (not l[i]%2):
            l[i] = l[i]//2
            i+=1
        else:
            del(l[i])
l = [1,2,3,4,5,6]
# print(modify_list(l))
# print(l)

def modify_list2(l):
    l[:] = [i//2 for i in l if(i%2) == 0]

print(modify_list2(l))
print(l)