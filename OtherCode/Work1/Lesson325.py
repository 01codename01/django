def get_int(start_mess, wrong_mess, end_mess):
    print(start_mess)
    while True:
        try:
            y = int(input())
            print(end_mess)
            return y
        except ValueError:
            print(wrong_mess)

x = get_int('Input int number:', 'Wrong value. Input int number:', 'Thank you.')
